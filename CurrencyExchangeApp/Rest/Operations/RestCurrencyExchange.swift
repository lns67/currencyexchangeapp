//
//  RestCurrencyExchange.swift
//  CurrencyExchangeApp
//
//  Created by DS on 01.11.2021.
//

import Foundation

//enum CodingKeys: String, CodingKey {
//    case value
//}

// MARK: - CurrencyExchange
class RestCurrencyExchange {
    
    func getAllCurrencies(completionHandler: @escaping ([String]?, String?) -> Void) {
        
        NetworkManager.shared.networkRequest(method: .get, urtlPath: URLs.listCurrenciesExchange) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }

            do {
                
                let decoder = JSONDecoder()
                let currencyArray = try? decoder.decode([String].self, from: data)
                completionHandler(currencyArray, nil)
                
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }
    }
    
    func count(from: String, to: String, q: String, completionHandler: @escaping (Double?, String?) -> Void) {
        let parametrs: [String : String] = ["from" : from, "to" : to, "q" : q]

        NetworkManager.shared.networkRequest(parametrs: parametrs, method: .get, urtlPath: URLs.currencyExchange) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }

            do {
                
                let decoder = JSONDecoder()
                let currencyArray = try? decoder.decode(Double.self, from: data)
                completionHandler(currencyArray, nil)
                
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }
    }
}
