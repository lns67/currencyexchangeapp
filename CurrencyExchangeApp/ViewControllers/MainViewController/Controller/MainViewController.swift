//
//  MainViewController.swift
//  CurrencyExchangeApp
//
//  Created by DS on 01.11.2021.
//

import UIKit

// MARK: - MainViewController
class MainViewController: UIViewController {

    @IBOutlet weak var entertedAmountTextField: UITextField!
    @IBOutlet weak var fromWhichCurrencyTextField: UITextField!
    
    @IBOutlet weak var resultTextField: UITextField!
    @IBOutlet weak var toWhichCurrencyTextField: UITextField!
    
    @IBOutlet weak var currencyPickreView: UIPickerView!
    
    var allCurrencies: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fromWhichCurrencyTextField.inputView = currencyPickreView
        toWhichCurrencyTextField.inputView = currencyPickreView
        
        dismissPickerView()
                
        RestCurrencyExchange().getAllCurrencies { currencies, error in
            
            guard error == nil else {
                self.present(AlertPopUp().showAlert(title: "Error", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }

            guard let currencies = currencies else {
                self.present(AlertPopUp().showAlert(title: "Error", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            self.allCurrencies = currencies
        }
    }
    
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        fromWhichCurrencyTextField.inputAccessoryView = toolBar
        toWhichCurrencyTextField.inputAccessoryView = toolBar
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    @IBAction func onExchangeButton(_ sender: Any) {
        
        guard
            let enteredAmount = entertedAmountTextField.text, !enteredAmount.isEmpty, (Double(enteredAmount) != nil),
            let fromCurrency = fromWhichCurrencyTextField.text, !fromCurrency.isEmpty,
            let toCurrency = toWhichCurrencyTextField.text, !toCurrency.isEmpty else {
            
            self.present(AlertPopUp().showAlert(title: "Error", message: "Check All Fields", buttonTitle: "OK"), animated: true, completion: nil)
            
            return
        }
        
        RestCurrencyExchange().count(from: fromCurrency, to: toCurrency, q: enteredAmount) { number, error in
            
            guard error == nil else {
                self.present(AlertPopUp().showAlert(title: "Error", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let number = number else {
                self.present(AlertPopUp().showAlert(title: "Error", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            self.resultTextField.text = String(number)
        }
    }
}
