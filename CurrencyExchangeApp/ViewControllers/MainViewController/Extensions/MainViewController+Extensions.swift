//
//  MainViewController+Extensions.swift
//  CurrencyExchangeApp
//
//  Created by DS on 01.11.2021.
//

import Foundation
import UIKit

// MARK: - MainViewController + Extensions

// MARK: - UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate
extension MainViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allCurrencies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allCurrencies[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
                
        if fromWhichCurrencyTextField.isEditing {
            fromWhichCurrencyTextField.text = allCurrencies[row]
        } else if toWhichCurrencyTextField.isEditing {
            toWhichCurrencyTextField.text = allCurrencies[row]
        }
    }
}
